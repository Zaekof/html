#coding: utf-8
#Nogier Loic
#tp3-ex7

n = input("Entrez un nombre entier: ")

fact = 1
i = 1
while i <= n:
    fact = fact * i
    i += 1
print "La factorielle de ",n," vaut: ",fact
