#coding: utf-8
#Nogier Loic

from math import *

#-- section exercice 1 --

class ft_Point():
    def __init__(self):
        self.nom=""
        self.x=0.0
        self.y=0.0
        self.z=0.0


#-- section exercice 2 --

    def ft_affichage(self):
        print self.nom,"=",self.x,";", self.y,";", self.z,"."


#-- section exercice 3 --

    def ft_saisir(self):
        self.nom = raw_input("Entrez le nom du point : ")
        self.x = input("Entrez la valeur de x : ")
        self.y = input("Entrez la valeur de y : ")
        self.z = input("Entrez la valeur de z : ")

#-- section exercice 4 --

    def ft_inf_ou_egal(self,P):
        if P.x < self.x: return True
        elif P.x > self.x: return False 
        elif P.y < self.y: return True
        elif P.y > self.y: return False
        elif P.z < self.z: return True
        elif P.z > self.z: return False
        else: return True   

#-- section exercice 5 --

    def ft_distance(self,g):
        return sqrt((self.x-g.x)**2+(self.y-g.y)**2+(self.z-g.z)**2)

#-- section exercice 6 --

class ft_collection_liste():
    def __init__(self):
        self.liste=[]

#-- section exercice 7 --

    def ft_ajouter(self,P):
        self.liste.append(P)

#-- section exercice 8 --

    def ft_afficher2(self):
        for i in range(len(self.liste)):
            ft_totpoint = self.liste[i]
            ft_totpoint.ft_affichage()

#-- section exercice 9 --
    def ft_identique(self,P):
        d = self.ft_distance(P)
        if (d < 0,00001):
            return True
        else:
            return False

    def ft_appartient(self,P):
        n = len(self.liste)
        for i in xrange(0, n):
            if (P.ft_identique(self.liste[i]) == True):
                return True
        return False

#-- section exercice 10 --

    def ft_intersection(self,C):
        r = ft_collection_liste()
        for e in self.liste:
            if (C.ft_appartient(e)):
                r.ft_ajouter(e)
        return r

#--section affichage--

ft_CollecPoint = ft_collection_liste()

Point = ft_Point()
Point.ft_saisir()
ft_CollecPoint.ft_ajouter(Point)

print "\n--- \t Séparation \t ---\n"

Point2 = ft_Point()
Point2.ft_saisir()
ft_CollecPoint.ft_ajouter(Point2)

ft_CollecPoint.ft_afficher2()